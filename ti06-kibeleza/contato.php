<?php

	require_once ("vendor/PHPMailerAutoload.php");

try{
	
    if(isset($_POST["email"])){
		
		$ok = 0;
		
        $assunto 	= "Site KiBeleza";
        $nome 		= $_POST["nome"];
        $email 		= $_POST["email"];
        $fone 		= $_POST["fone"];
        $mens	 	= $_POST["mens"];
		        
                   
     
        $phpmail = new PHPMailer(); // Instânciamos a classe PHPmailer para poder utiliza-la  
		
        $phpmail->isSMTP(); // envia por SMTP
        
        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';
        
        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL
        
        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   
        
        $phpmail->Username = "smpsistema@gmail.com"; // SMTP username         
        $phpmail->Password = "******"; // SMTP password
		
        $phpmail->IsHTML(true);         
        
        $phpmail->setFrom($email, $nome); // E-mail do remetende enviado pelo method post  
                 
        $phpmail->addAddress("smpsistema@gmail.com", $assunto);// E-mail do destinatario/*  
        
        $phpmail->Subject = $assunto; // Assunto do remetende enviado pelo method post
                
        $phpmail->msgHTML(" Nome: $nome <br>
                            E-mail: $email <br>
                            Telefone: $fone <br>
                            Mensagem: $mens ");
						  						       
        $phpmail->AlrBody = "Nome: $nome \n
                            E-mail: $email \n
                            Telefone: $fone \n
                            Mensagem: $mens";
            
        if($phpmail->send()){ 
			$ok = 1; 
        }else{
			$ok = 2;
        }
		         
        // ############## RESPOSTA AUTOMATICA
        $phpmailResposta = new PHPMailer();        
        $phpmailResposta->isSMTP();
        
        $phpmailResposta->SMTPDebug = 0;
        $phpmailResposta->Debugoutput = 'html';
        
        $phpmailResposta->Host = "smtp.gmail.com";         
        $phpmailResposta->Port = 587;
        
        $phpmailResposta->SMTPSecure = 'tls';
        $phpmailResposta->SMTPAuth = true;   
        
        $phpmailResposta->Username = "smpsistema@gmail.com";         
        $phpmailResposta->Password = "******";          
        $phpmailResposta->IsHTML(true);         
        
        $phpmailResposta->setFrom($email, $nome); // E-mail do remetende enviado pelo method post  
                 
        $phpmailResposta->addAddress($email, $assunto);// E-mail do destinatario/*  
        
        $phpmailResposta->Subject = "Resposta - " .$assunto; // Assunto do remetende enviado pelo method post
                
        $phpmailResposta->msgHTML(" Nome: $nome <br>
                            Em breve daremos o retorno");
        
        $phpmailResposta->AlrBody = "Nome: $nome \n
                            Em breve daremos o retorno";
            
        $phpmailResposta->send();
        
    }
    
}catch(Exception $e){
     Erro::tratarErro($e); 
}
    
?>

<!doctype html>  <!-- Info pág HTML5 -->
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta name="Description" content="Site KiBeleza">
	<meta name="Keywords" content="Beleza, Cortes">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>.: KiBeleza TI06 :.</title>
	<!-- CSS ANIMATE -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- CSS SLICK -->
	<link rel="stylesheet" type="text/css" href="css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
	
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/minhasAnimacoes.css">
	<link rel="stylesheet" href="css/estilo.css">
</head>

<body><!-- CORPO -->
	
	<!-- AQUI É O TOPO -->
	<?php require_once("topo.php") ?>
	
	<section>
	
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3659.0254647270517!2d-46.434050784408164!3d-23.495592265053293!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce63dda7be6fb9%3A0xa74e7d5a53104311!2sSenac%20S%C3%A3o%20Miguel%20Paulista!5e0!3m2!1spt-BR!2sbr!4v1581445872497!5m2!1spt-BR!2sbr" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	
	</section>
	
	<section class="form site">
		<article>
			<h2>Formulário de Contato</h2>
			
			<form method="post" action="#">
	<div><input name="nome" type="text" placeholder="Nome: "></div>
				
	<div><input name="email" type="email" placeholder="Email: " required></div>
				
	<div><input name="fone" type="tel" placeholder="Telefone: "></div>
				
	<div><textarea name="mens" cols="10" rows="20" placeholder="Digite sua mensagem"></textarea></div>
				
	<div>
		<span>
			<?php
				if(@$ok == 1){
					echo $nome. ", a mensagem foi enviada com sucesso.";
				}else if(@$ok == 2){
					echo $nome. ", não foi possível enviar a mensagem. Erro: " .$phpmail->ErrorInfo;
				}
			?>
		</span>
		
		<input type="submit" value="ENVIAR">
				
	</div>	
				
			</form>
			
		</article>
		<article>
			<img src="img/contato.jpg" alt="Contato">
		</article>
	</section>
		
	<div class="faixaTopo">
		<?php require_once("insta.php") ?>	
	</div>
	
	<!-- RODAPÉ -->
	<?php require_once("rodape.php") ?>
	
	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/slick.js"></script>
	<script src="js/animacoes.js"></script>
</body><!-- FIM CORPO -->
</html>





