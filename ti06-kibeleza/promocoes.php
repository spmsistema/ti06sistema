<!doctype html>  <!-- Info pág HTML5 -->
<html lang="pt-br">

<head>
	<meta charset="utf-8">
	<meta name="Description" content="Site KiBeleza">
	<meta name="Keywords" content="Beleza, Cortes">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>.: KiBeleza TI06 :.</title>
	<!-- CSS ANIMATE -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- CSS SLICK -->
	<link rel="stylesheet" type="text/css" href="css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/slick-theme.css"/>
	
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/minhasAnimacoes.css">
	<link rel="stylesheet" href="css/estilo.css">
</head>

<body><!-- CORPO -->
	
	
	<!-- AQUI É O TOPO -->
	<?php require_once("topo.php") ?>
	
	<?php require_once("banner.php") ?>
	
	<div class="faixaTopo">
		<?php require_once("insta.php") ?>
	</div>
	
	<!-- RODAPÉ -->
	<?php require_once("rodape.php") ?>
	
	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/slick.js"></script>
	<script src="js/animacoes.js"></script>
</body><!-- FIM CORPO -->
</html>





