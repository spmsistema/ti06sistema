// JavaScript Document

// ########Animações WOW
new WOW().init();

// ########BANNER
$('.banner').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  dots:true,
  autoplaySpeed: 2000,
});

// ########## MENU
document.querySelector(".abrir-menu").onclick = function(){
	//console.log("Teste do botão menu");
	document.documentElement.classList.add("menu-ativo");
}

document.querySelector(".fechar-menu").onclick = function(){
	//console.log("Teste do botão menu");
	document.documentElement.classList.remove("menu-ativo");
}

// ########### TOPO FIXO

window.onscroll = function(){

	var top = window.pageYOffset || document.documentElement.scrollTop;
	console.log(top);
	
	if(top > 500){
		console.log("Menu fixo em 500");
		document.getElementById("topo-fixo").classList.add("menu-fixo");
	}else{
		console.log("Menu abaixo de 500");
		document.getElementById("topo-fixo").classList.remove("menu-fixo");
	}
	
}
















