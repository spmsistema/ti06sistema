<header><!-- TOPO -->
		<div class="faixaTopo">
			<div class="site contatoSocial"> <!-- TOPO CONTATO -->
				<h2 class="wow fadeInDown">contato@kibeleza.com.br</h2>
				<ul class="wow fadeInDown">
					<li><a href="#"><img src="img/faceBranco.svg" alt="Logo Facebook"></a></li>
					<li><a href="#"><img src="img/InstaBranco.svg" alt="Logo Instagram"></a></li>
					<li><a href="#"><img src="img/twitterBranco.svg" alt="Logo Twttir"></a></li>
				</ul>
			</div>
		</div>
		<div id="topo-fixo"> <!-- TOPO LOGO e MENU -->
			<div class="site logoMenu">
				<h1 class="wow pulse">Logo KiBeleza</h1>
				<button class="abrir-menu"></button>
				<nav>
					<button class="fechar-menu"></button>
					<ul>
						<li><a href="index.php">HOME</a></li>
						<li><a href="sobre.php">SOBRE</a></li>
						<li><a href="servico.php">SERVIÇO</a></li>
						<li><a href="promocoes.php">PROMOÇÕES</a></li>
						<li><a href="#">BLOG</a></li>
						<li><a href="contato.php">CONTATO</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>