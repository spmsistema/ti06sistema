<footer class="rodape">
		<img src="img/LogoKiBelezaBranco.svg" alt="Logo KiBeleza" class="wow fadeInUp">
		<p class="wow fadeInUp">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		<ul class="wow fadeInUp">
			<li><a href="#"><img src="img/faceBranco.svg" alt="Facebook"></a></li>
			<li><a href="#"><img src="img/InstaBranco.svg" alt="Instagram"></a></li>
			<li><a href="#"><img src="img/twitterBranco.svg" alt="Twitter"></a></li>
			<li><a href="#"><img src="img/WhatsBranco.svg" alt="WhatsAPP"></a></li>
		</ul>
		<div>
			<h2>© Todos os direitos reservados - <a href="http://www.supremodigital.com.br" target="_blank"> Allê Palmeira</a></h2>
		</div>
	</footer>